#ifndef __EVOLUTION_H__
#define __EVOLUTION_H__

double A(int L,int M,int Lprime,int Mprime,int fancyL,int fancyM);

int laser_time_evolution(double t,const double y[],double dydt[],void *params __attribute__((unused)),int L1,int L2);
int time_evolution(double t,const double y[],double dydt[],void *params);

void do_dynamics(int nr_steps, double ti, double tf);

#endif //__EVOLUTION_H__
