#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "config.h"
#include "centrifuge.h"
#include "basis.h"
#include "auxx.h"
#include "inih/ini.h"

int configuration_handler(void *user __attribute__((unused)), const char *section, const char *name, const char *value)
{

#define MATCH(s, n) (strcmp(section,s)==0)&&(strcmp(name,n)==0)

	if(MATCH("general", "Lmax"))
	{
		Lmax=atoi(value);
	}
	else if(MATCH("general", "startL"))
	{
		startL=atoi(value);
	}
	else if(MATCH("general", "startM"))
	{
		startM=atoi(value);
	}
	else if(MATCH("grid", "qsteps"))
	{
		qsteps=atoi(value);
	}
	else if(MATCH("grid", "qcutoff"))
	{
		qcutoff=atof(value);
	}
	else if(MATCH("grid", "basis"))
	{
		if(strcmp(value, "grid")==0)
			kbasis=KBASIS_GRID;
		else if(strcmp(value, "legendre")==0)
			kbasis=KBASIS_LEGENDRE;
		else if(strcmp(value, "hermite")==0)
			kbasis=KBASIS_HERMITE;
		else if(strcmp(value, "boxes")==0)
			kbasis=KBASIS_BOXES;
	}
	else if(MATCH("molecule", "BinGHz"))
	{
		BinGHz=atof(value);
	}
	else if(MATCH("molecule", "bathdensity"))
	{
		n=atof(value);
	}
	else if(MATCH("molecule", "u0"))
	{
		u0=atof(value);
	}
	else if(MATCH("molecule", "r0"))
	{
		r0=atof(value);
	}
	else if(MATCH("molecule", "u2"))
	{
		u2=atof(value);
	}
	else if(MATCH("molecule", "r2"))
	{
		r2=atof(value);
	}
	else if(MATCH("centrifuge", "rate"))
	{
		rate=atof(value);
	}
	else if(MATCH("centrifuge", "U0"))
	{
		parameterU0=atof(value);
	}
	else if(MATCH("evolution", "nrsteps"))
	{
		nrsteps=atoi(value);
	}
	else if(MATCH("evolution", "tstart"))
	{
		tstart=atof(value);
	}
	else if(MATCH("evolution", "tend"))
	{
		tend=atof(value);
	}
	else if(MATCH("evolution", "restricted"))
	{
		if(!strcasecmp(value, "true"))
			restricted=true;
		else
			restricted=false;
	}
	else if(MATCH("output", "file"))
	{
		if(strcmp(value,"description")==0)
		{
			/*
				The output file format, as requested by Igor.
			*/

			prefix=malloc(sizeof(char)*1024);

			sprintf(prefix, "s-L%d-M%d-Lmax%d-beta%g-den%g-u0%g-f%g-st%d", startL, startM, Lmax, rate, n,
				u0, parameterU0, nrsteps);

			prefix[1023]='\0';

		}
		else if(strcmp(value, "auto")==0)
		{
			if(!strstr(inipath, ".ini"))
			{
				printf("Error: using automatic prefix, but the configuration file path does not contain '.ini'\n");
				exit(0);
			}

			/*
				As an alternative, one could just have the usual 'auto' behaviour.
			*/

			prefix=find_and_replace(inipath,".ini","");
		}
		else
		{
			prefix=strdup(value);
		}
	}
	else
	{
		return 0;  /* unknown section/name, error */
	}

	return 1;
}

void load_configuration_defaults(void)
{
	/*
		We initialize the required parameters:

		Lmax: maximum L in the superposition
		startL, startM: the initial (L,M) state

		qsteps: number of states in the basis we use for k (points in the grid, or Legendre polynomials, etc...)
		qcutoff: k-cutoff
		kbasis: can be KBASIS_GRID, KBASIS_LEGENDRE, KBASIS_HERMITE or KBASIS_BOXES

		BinGHz: the rotational constant of the molecule, in GHz
		n: the density of the bath, in 'angulon' units
		u0,r0,u1,r1,u2,r2: parameters of the angulon potential (note that \lambda=1 is not actually used)
	*/

	Lmax=48;
	startL=2;
	startM=0;

	qsteps=2;
	qcutoff=12.0f;
	kbasis=KBASIS_GRID;

	BinGHz=30.0f;
	n=5.0f;

	u0=500.0f;
	r0=1.5f;
	u1=0.0f;
	r1=0.0f;
	u2=500.0f;
	r2=1.5f;

	nrsteps=1000;
	tstart=0.0f;
	tend=2.0f;
	restricted=true;

	rate=1.0f;
	parameterU0=1.0f;

	prefix="centrifuge";
}

bool do_ini_file(char *inifile)
{
	load_configuration_defaults();
	inipath=inifile;

	if(ini_parse(inifile, configuration_handler, NULL)<0)
	{
		printf("Can't load '%s'\n", inifile);
		return false;
	}

	char fname[1024];

	snprintf(fname, 1024, "%s.dat", prefix);
	fname[1023]='\0';

	outfile=fopen(fname, "w+");

	return true;
}
