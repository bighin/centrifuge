#include <math.h>
#include <stdlib.h>
#include <assert.h>
#include <stdbool.h>

#include <gsl/gsl_integration.h>
#include <gsl/gsl_sf.h>

#include "basis.h"
#include "centrifuge.h"

/*
	Helper functions, particularly easy if we use a grid.
*/

double falpha_grid(int lambda,int q)
{
	assert((lambda==0)||(lambda==2));

	assert(q>=0);
	assert(q<qsteps);

	/*
		The next part depends on the choice of the basis
	*/

	double k=(q+0.5f)*(qcutoff/qsteps);

	if(q==0)
		return 0.0f;

	return U(lambda,k)*sqrt(qcutoff/qsteps);
}

double fgamma_grid(int q,int qprime)
{
	assert(q>=0);
	assert(q<qsteps);

	assert(qprime>=0);
	assert(qprime<qsteps);

	/*
		The next part depends on the choice of the basis
	*/

	if(q!=qprime)
		return 0.0f;

	double k=(q+0.5)*(qcutoff/qsteps);

	return omegak(k);
}

/*
	Helper functions in case we use a basis for discretizing k; basis_H() will return the q-th
	function in the orthonormal basis we have chosen, evaluated at k.
*/

double fact(int nn)
{
	return tgamma(nn+1);
}

double basis_H(int q,double k)
{
	if(kbasis==KBASIS_HERMITE)
	{
		double x=1.0f;
		double actualk=k/x;

		return sqrt(actualk/k)*sqrt(2.0f)*pow(pow(2.0f,2*q)*fact(2*q)*sqrt(M_PI),-0.5f)*exp(-actualk*actualk/2.0f)*gsl_sf_hermite_phys(2*q,actualk);
	}

	else if(kbasis==KBASIS_LEGENDRE)
	{
		int l=1+2*q;
		return sqrt(2.0f*l+1.0f)*gsl_sf_legendre_Pl(l,k/qcutoff)/sqrt(qcutoff);
	}

	else if(kbasis==KBASIS_BOXES)
	{
		double lo,hi;

		lo=q*qcutoff/qsteps;
		hi=(q+1)*qcutoff/qsteps;

		if((k>=lo)&&(k<hi))
			return 1.0f/sqrt(qcutoff/qsteps);

		return 0.0f;
	}

	assert(false);
	return 0.0f;
}

/*
	We use some tables to precalculate the values of the 'alpha' and 'gamma' integrals,
	see Igor's notes.
*/

double *basis_table_alpha_0;
double *basis_table_alpha_2;
double *basis_table_gamma;

double falpha_basis(int lambda, int q)
{
	assert((lambda==0)||(lambda==2));
	assert(q>=0);
	assert(q<qsteps);

	if(lambda==0)
		return basis_table_alpha_0[q];

	else if(lambda==2)
		return basis_table_alpha_2[q];

	assert(false);
	return 0.0f;
}

double fgamma_basis(int q, int qprime)
{
	assert(q>=0);
	assert(q<qsteps);
	assert(qprime>=0);
	assert(qprime<qsteps);

	return basis_table_gamma[q*qsteps+qprime];
}

/*
	These functions are used to precalculate the tables when the program starts.
*/

struct params_t
{
	int L,M;
	int lambda,q,qprime;
};

double falpha_basis_integrand(double k, void *params)
{
	struct params_t *p=params;

	return basis_H(p->q,k)*U(p->lambda,k);
}

double precalc_falpha_basis(int lambda, int q)
{
	struct params_t p;
	gsl_function F;

	double result,error;

	assert((lambda==0)||(lambda==2));
	assert(q>=0);
	assert(q<qsteps);

	gsl_integration_workspace *w=gsl_integration_workspace_alloc(50000);

	F.function=&falpha_basis_integrand;

	p.lambda=lambda;
	p.q=q;
	F.params=&p;

	gsl_integration_qag(&F,0.0f,qcutoff,0.1,0.1,50000,GSL_INTEG_GAUSS61,w,&result,&error);

	gsl_integration_workspace_free(w);

	return result;
}

double fgamma_basis_integrand(double k, void *params)
{
	struct params_t *p=params;

	return basis_H(p->q,k)*basis_H(p->qprime,k)*omegak(k);
}

double precalc_fgamma_basis(int q, int qprime)
{
	struct params_t p;
	gsl_function F;

	double result,error;

	assert(q>=0);
	assert(q<qsteps);
	assert(qprime>=0);
	assert(qprime<qsteps);

	gsl_integration_workspace *w=gsl_integration_workspace_alloc(50000);

	F.function=&fgamma_basis_integrand;

	p.q=q;
	p.qprime=qprime;
	F.params=&p;

	gsl_integration_qag(&F,0.0f,qcutoff,0.1,0.1,50000,GSL_INTEG_GAUSS61,w,&result,&error);

	gsl_integration_workspace_free(w);

	return result;
}

double normalization_integrand(double k,void *params)
{
	struct params_t *p=params;

	return basis_H(p->q,k)*basis_H(p->qprime,k);
}

double check_normalization(int q,int qprime)
{
	struct params_t p;
	gsl_function F;

	double result,error;

	assert(q>=0);
	assert(q<qsteps);
	assert(qprime>=0);
	assert(qprime<qsteps);

	gsl_integration_workspace *w=gsl_integration_workspace_alloc(50000);

	F.function=&normalization_integrand;

	p.q=q;
	p.qprime=qprime;
	F.params=&p;

	gsl_integration_qag(&F,0.0f,qcutoff,0.1,0.1,50000,GSL_INTEG_GAUSS61,w,&result,&error);

	gsl_integration_workspace_free(w);

	return result;
}

void init_basis_tables(void)
{
	basis_table_alpha_0=malloc(sizeof(double)*qsteps);
	basis_table_alpha_2=malloc(sizeof(double)*qsteps);
	basis_table_gamma=malloc(sizeof(double)*qsteps*qsteps);

	assert(basis_table_alpha_0);
	assert(basis_table_alpha_2);
	assert(basis_table_gamma);

	for(int q=0;q<qsteps;q++)
	{
		basis_table_alpha_0[q]=precalc_falpha_basis(0, q);
		basis_table_alpha_2[q]=precalc_falpha_basis(2, q);
	}

	for(int q=0;q<qsteps;q++)
	{
		for(int qprime=0;qprime<qsteps;qprime++)
		{
			basis_table_gamma[q*qsteps+qprime]=precalc_fgamma_basis(q, qprime);
		}
	}
}

void fini_basis_tables(void)
{
	if(basis_table_alpha_0)
		free(basis_table_alpha_0);

	if(basis_table_alpha_2)
		free(basis_table_alpha_2);

	if(basis_table_gamma)
		free(basis_table_gamma);
}

const char *get_basis_type(void)
{
	static const char *desc_grid="grid";
	static const char *desc_legendre="Legendre polynomials";
	static const char *desc_hermite="Hermite functions";
	static const char *desc_boxes="boxes";
	static const char *desc_null="(null)";

	switch(kbasis)
	{
		case KBASIS_GRID:
			return desc_grid;

		case KBASIS_LEGENDRE:
			return desc_legendre;

		case KBASIS_HERMITE:
			return desc_hermite;

		case KBASIS_BOXES:
			return desc_boxes;

		default:
			break;
	}

	assert(false);
	return desc_null;
}
