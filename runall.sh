#!/bin/bash

for i in inifiles/tpl*.ini ;
do
    OUTFILE=$(echo $i | sed "s/\.ini/\.log/g")
    sbatch --export=INIFILE=$i --output=$OUTFILE centrifuge.sbatch
    echo $i
done
