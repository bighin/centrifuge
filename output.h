#ifndef __OUTPUT_H__
#define __OUTPUT_H__

void myprintf(const char *fmt, ...);

void print_header(void);
void print_first_line(double t, double *y);
void print_entry_line(double t, double y[]);
void print_footer(double t,double y[]);

#endif //__OUTPUT_H__
