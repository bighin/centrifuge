#include <stdio.h>
#include <math.h>
#include <complex.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <assert.h>

#include <gsl/gsl_sf.h>

#include "centrifuge.h"
#include "basis.h"
#include "output.h"
#include "inih/ini.h"
#include "auxx.h"

/*
	Global variables, they will be set at the beginning of the program after
	reading the .ini file
*/

int Lmax;
int startL, startM;
int qsteps;
double qcutoff;
int dims;

int kbasis;

double n;
double BinGHz;
double u0, r0;
double u1, r1;
double u2, r2;

double parameterU0, rate;

int nrsteps;
double tstart, tend;
bool restricted;

char *inipath, *prefix;
FILE *outfile;

/*
	Wigner's 3j symbol from the GNU Scientific Library
*/

double wigner3j(int l1, int l2, int l3, int m1, int m2, int m3)
{
	return gsl_sf_coupling_3j(2*l1, 2*l2, 2*l3, 2*m1, 2*m2, 2*m3);
}

double cg_no_tables(int j1, int m1, int j2, int m2, int j, int m)
{
	double phase;

	if(((j1-j2+m)%2)==0)
		phase=1.0f;
	else
		phase=-1.0f;

	return phase*sqrt(2.0f*j+1.0f)*wigner3j(j1, j2, j, m1, m2, -m);
}

/*
	Functions for dealing with tables of Clebsch-Gordan coefficients.

	Note that j2 (notation as used by the function cg_no_tables()) can only take the values 0 and 2.
	We do not precalculate m, since its only possible value is enforced by m-conservation.
*/

#define CGTABLES_LMAX        (60)

double cgtables[CGTABLES_LMAX][2*CGTABLES_LMAX+1][6][CGTABLES_LMAX];

bool cg_fill_tables(void)
{
	for(int j1=0;j1<CGTABLES_LMAX;j1++)
	{
		for(int m1=-j1;m1<=j1;m1++)
		{
			for(int j=0;j<CGTABLES_LMAX;j++)
			{
				cgtables[j1][m1+j1][0][j]=cg_no_tables(j1, m1, 0, 0, j, m1+0);

				cgtables[j1][m1+j1][1][j]=cg_no_tables(j1, m1, 2, -2, j, m1-2);
				cgtables[j1][m1+j1][2][j]=cg_no_tables(j1, m1, 2, -1, j, m1-1);
				cgtables[j1][m1+j1][3][j]=cg_no_tables(j1, m1, 2, 0, j, m1+0);
				cgtables[j1][m1+j1][4][j]=cg_no_tables(j1, m1, 2, 1, j, m1+1);
				cgtables[j1][m1+j1][5][j]=cg_no_tables(j1, m1, 2, 2, j, m1+2);
			}
		}
	}

	return true;
}

int cg_tables_dimensions(void)
{
	return sizeof(double)*(CGTABLES_LMAX)*(2*CGTABLES_LMAX+1)*6*(CGTABLES_LMAX);
}

double cg(int j1, int m1, int j2, int m2, int j, int m)
{
	if(m!=(m1+m2))
		return 0.0f;

	if((j1<CGTABLES_LMAX)&&(j2<CGTABLES_LMAX)&&(j<CGTABLES_LMAX))
	{
		if((abs(m1)>j1)||(abs(m2)>j2)||(abs(m)>j))
		{
			assert(0.0f==cg_no_tables(j1, m1, j2, m2, j, m));
			return 0.0f;
		}

		if(j2==0)
		{
			assert(cgtables[j1][m1+j1][0][j]==cg_no_tables(j1, m1, j2, m2, j, m));
			return cgtables[j1][m1+j1][0][j];
		}

		if(j2==2)
		{
			assert(cgtables[j1][m1+j1][1+2+m2][j]==cg_no_tables(j1, m1, j2, m2, j, m));
			return cgtables[j1][m1+j1][1+2+m2][j];
		}
	}

	return cg_no_tables(j1, m1, j2, m2, j, m);
}

/*
	Dispersion relations for free particles and Bogoliubov quasiparticles
*/

double ek(double k)
{
	return k*k/2.0f;
}

double omegak(double k)
{
	double num, den;
	double a0, a1, a2, a3, a4, a5, b0, b1, b2, b3, b4, b5;

	double kscaled=k*sqrt(BinGHz/1.1178);

	/*
		Padè approximant for the dispersion relation: coefficients for I2
	*/

	a0=0.0f;
	a1=78979.82692539008;
	a2=-4453.106390752133;
	a3=383.5583835895215;
	a4=-21.92511621355489;
	a5=0.3824072227009381;

	b0=3433.7252161679503;
	b1=-218.02760941323945;
	b2=12.467652732020886;
	b3=0.5009420138478766;
	b4=-0.05760613914422612;
	b5=0.0010925920648598231;

	num=a0+kscaled*(a1+kscaled*(a2+kscaled*(a3+kscaled*(a4+kscaled*a5))));
	den=b0+kscaled*(b1+kscaled*(b2+kscaled*(b3+kscaled*(b4+kscaled*b5))));

	return num/den*(1.1178/BinGHz);
}

/*
	Dawson's integral, from: http://www.ebyte.it/library/codesnippets/DawsonIntegralApproximations.html
        (maximum relative error: 10 ppm)
*/

double DawsonF(double x)
{
	double y, p, q;

	y=x*x;

	p=1.0+y*(0.1049934947+y*(0.0424060604+y*(0.0072644182+y*(0.0005064034+y*(0.0001789971)))));

	q=1.0+y*(0.7715471019+y*(0.2909738639+y*(0.0694555761+y*(0.0140005442+y*(0.0008327945+2*0.0001789971*y)))));

	return x*(p/q);
}

/*
	Potentials
*/

double U0(double k)
{
	double a, b, c;

	a=sqrt((8.0f*n*k*k*ek(k))/(omegak(k)));
	b=exp(-0.5f*k*k*r0*r0);
	c=4*M_PI*pow(r0, -3.0f);

	return u0*a*b/c;
}

double U1(double k)
{
	double a, b, c;

	a=sqrt((8.0f*n*k*k*ek(k))/(3.0f*omegak(k)));
	b=r1*(-sqrt(2.0f)*k*r1+2.0f*(1+k*k*r1*r1)*DawsonF(k*r1/sqrt(2.0f)));
	c=4*pow(M_PI, 3.0f/2.0f)*k*k;

	return u1*a*b/c;
}

double U2(double k)
{
	double a, b, c;

	a=sqrt((8.0f*n*k*k*ek(k))/(5.0f*omegak(k)));
	b=-2.0f*exp(-0.5f*k*k*r2*r2)*k*r2*(3.0f+k*k*r2*r2)+3*sqrt(2.0f*M_PI)*erf(k*r2/sqrt(2.0f));
	c=8.0f*k*k*k*M_PI;

	return u2*a*b/c;
}

double U(int lambda, double k)
{
	switch(lambda)
	{
		case 0:
			return U0(k);

		case 1:
			return U1(k);

		case 2:
			return U2(k);

		default:
			assert(false);
	}

	return 0.0f;
}

/*
	Indices for the real and imaginary part of g and beta.
*/

int get_g_index(int type, int L __attribute__((unused)), int M __attribute__((unused)))
{
	assert((L>=0)&&(L<Lmax));
	assert(abs(M)<=L);

	assert((type==REAL)||(type==IMAG));

	if(type==IMAG)
		return 1;

	return 0;
}

int get_beta_index(int type, int L, int M __attribute__((unused)), int lambda, int j, int q)
{
	int index=0;

	assert((L>=0)&&(L<Lmax));
	assert(abs(M)<=L);

	assert((type==REAL)||(type==IMAG));

	assert((lambda==0)||(lambda==2));
	assert(((j==(L-2))&&((L-2)>=0))||(j==L)||(j==(L+2)));

	assert(q>=0);
	assert(q<qsteps);

	if(lambda==0)
	{
		assert(j==L);
	}

	if(lambda==2)
	{
		index+=2;

		if(j==L)
			index+=2;

		if(j==(L+2))
			index+=4;
	}

	if(type==IMAG)
		index+=1;

	return 2+8*q+index;
}

/*
	General 'alpha' and 'gamma' functions, calling either the grid or
	the Legendre version.
*/

double falpha(int lambda, int q)
{
	switch(kbasis)
	{
		case KBASIS_GRID:
			return falpha_grid(lambda, q);

		case KBASIS_LEGENDRE:
		case KBASIS_HERMITE:
		case KBASIS_BOXES:
			return falpha_basis(lambda, q);

		default:
			break;
	}

	assert(false);
	return 0.0f;
}

double fgamma(int q, int qprime)
{
	switch(kbasis)
	{
		case KBASIS_GRID:
			return fgamma_grid(q, qprime);

		case KBASIS_LEGENDRE:
		case KBASIS_HERMITE:
		case KBASIS_BOXES:
			return fgamma_basis(q, qprime);

		default:
			break;
	}

	assert(false);
	return 0.0f;
}

int get_LM_index(int L, int M)
{
	assert(L>=0);
	assert(L<Lmax);
	assert(abs(M)<=L);

	return L*L+M+L;
}

double get_qpw_L(const double y[], int L, int M)
{
	double complex g=y[get_g_index(REAL, L, M)]+I*y[get_g_index(IMAG, L, M)];

	return sqrt(g*conj(g));
}

double get_qpw(const double y[])
{
	double result=0.0f;

	for(int L=0;L<Lmax;L++)
	{
		for(int M=-L;M<=L;M++)
		{
			int offset=get_LM_index(L, M)*dims;

			result+=get_qpw_L(&y[offset], L, M);
		}
	}

	return result;
}

double get_energy_L(const double y[], int L, int M)
{
	double complex A, B, C;

	double complex g=y[get_g_index(REAL, L, M)]+I*y[get_g_index(IMAG, L, M)];

	A=g*conj(g)*L*(L+1);

	B=0.0f;
	for(int lambda=0;lambda<=2;lambda+=2)
	{
		for(int j=L-2;j<=L+2;j+=2)
		{
			if(j<0)
				continue;

			if((lambda==0)&&(j!=L))
				continue;

			for(int q=0;q<qsteps;q++)
			{
				for(int qprime=0;qprime<qsteps;qprime++)
				{
					double complex betaq, betaqprime;

					betaq=y[get_beta_index(REAL, L, M, lambda, j, q)]+
						I*y[get_beta_index(IMAG, L, M, lambda, j, q)];
					betaqprime=y[get_beta_index(REAL, L, M, lambda, j, qprime)]+
						I*y[get_beta_index(IMAG, L, M, lambda, j, qprime)];

					if(q==qprime)
						B+=conj(betaq)*betaqprime*j*(j+1);

					B+=conj(betaq)*betaqprime*fgamma(q, qprime);
				}
			}
		}
	}

	C=0.0f;
	for(int lambda=0;lambda<=2;lambda+=2)
	{
		for(int j=L-2;j<=L+2;j+=2)
		{
			if(j<0)
				continue;

			if((lambda==0)&&(j!=L))
				continue;

			for(int q=0;q<qsteps;q++)
			{
				double complex betaq;
				double coupling;

				betaq=y[get_beta_index(REAL, L, M, lambda, j, q)]+
					I*y[get_beta_index(IMAG, L, M, lambda, j, q)];
				coupling=cg(j, 0, lambda, 0, L, 0)*
					sqrt((2.0f*j+1.0f)*(2.0f*lambda+1.0f)/(2.0f*L+1.0f)/(4.0f*M_PI));

				C+=conj(betaq)*falpha(lambda, q)*coupling*g;
			}
		}
	}

	return A+B+C+conj(C);
}

double get_energy(const double y[])
{
	double result=0.0f;

	for(int L=0;L<Lmax;L++)
	{
		for(int M=-L;M<=L;M++)
		{
			int offset=get_LM_index(L, M)*dims;

			result+=get_energy_L(&y[offset], L, M);
		}
	}

	return result;
}

double get_norm_LM(const double y[], int L, int M)
{
	double complex g=y[get_g_index(REAL, L, M)]+I*y[get_g_index(IMAG, L, M)];

	double ret=g*conj(g);

	for(int lambda=0;lambda<=2;lambda+=2)
	{
		for(int j=L-2;j<=L+2;j+=2)
		{
			if(j<0)
				continue;

			if((lambda==0)&&(j!=L))
				continue;

			for(int q=0;q<qsteps;q++)
			{
				double complex betaq=y[get_beta_index(REAL, L, M, lambda, j, q)]+
					I*y[get_beta_index(IMAG, L, M, lambda, j, q)];

				ret+=conj(betaq)*betaq;
			}
		}
	}

	return ret;
}

double get_norm_L(const double y[], int L)
{
	double ret=0.0f;

	for(int M=-L;M<=L;M++)
	{
		int offset=get_LM_index(L, M)*dims;

		ret+=get_norm_LM(&y[offset], L, M);
	}

	return ret;
}

double get_norm(const double y[])
{
	double ret=0.0f;

	for(int L=0;L<Lmax;L++)
	{
		for(int M=-L;M<=L;M++)
		{
			int offset=get_LM_index(L, M)*dims;

			ret+=get_norm_LM(&y[offset], L, M);
		}
	}

	return ret;
}
