#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <complex.h>

#include "centrifuge.h"
#include "output.h"
#include "basis.h"

#define BUFLEN	(1024)

void myprintf(const char *fmt, ...)
{
	char *p;
	va_list ap;

	if((p=malloc(sizeof(char)*BUFLEN))==NULL)
		return;

	va_start(ap, fmt);
	vsnprintf(p, BUFLEN, fmt, ap);
	va_end(ap);

	printf("%s",p);
	fflush(stdout);

	if(outfile)
	{
		fprintf(outfile,"%s",p);
		fflush(outfile);
	}

	if(p)
		free(p);
}


void print_header(void)
{
	myprintf("# Weak-coupling dynamics\n");
	myprintf("#\n");
	myprintf("# Potential:  u0 = %f\n", u0);
	myprintf("#             r0 = %f\n", r0);
	myprintf("#             u2 = %f\n", u2);
	myprintf("#             r2 = %f\n", r2);
	myprintf("#\n");
	myprintf("# Laser:      U0    = %f\n", parameterU0);
	myprintf("#             rate  = %f\n", rate);
	myprintf("#\n");
	myprintf("# Density:    n  = %f\n", n);
	myprintf("#\n");
	myprintf("# Maximum L:  Lmax = %d\n", Lmax);
	myprintf("#\n");
	myprintf("# Initial state:  (L,M) = (%d,%d)\n", startL, startM);
	myprintf("#\n");
	myprintf("# Rotational constant: B = %f GHz\n", BinGHz);
	myprintf("#\n");
	myprintf("# Dispersion relation: experimental (Helium)\n");
	myprintf("#\n");
	myprintf("# Discretization: basis  = %s\n", get_basis_type());
	myprintf("#                 steps  = %d\n", qsteps);
	myprintf("#                 cutoff = %f\n", qcutoff);
	myprintf("#\n");
	myprintf("# Time: starting = %f\n", tstart);
	myprintf("#       ending = %f\n", tend);
	myprintf("#       number of steps = %d\n", nrsteps);
	myprintf("#\n");
	myprintf("# Configuration read from: %s\n", inipath);
	myprintf("# Output goes to: %s.dat\n", prefix);
	myprintf("#\n");
	myprintf("# Differential system dimensions: %d\n", dims*Lmax*Lmax);
	myprintf("#\n");
}

void print_first_line(double t __attribute__((unused)), double *y __attribute__((unused)))
{
	myprintf("# <t> <|g(L=0)|^2> ...<|g(L=Lmax)|^2> <norm>\n");
}

void print_entry_line(double t,double y[])
{
	double expvalL=0.0f;

	for(int L=0;L<Lmax;L++)
		expvalL+=L*get_norm_L(y,L);

	myprintf("%f ", t);

	for(int L=0;L<Lmax;L++)
	{
		double total=0.0f;

		for(int M=-L;M<=L;M++)
		{
			double complex g=y[get_LM_index(L,M)*dims+get_g_index(REAL,L,M)]+
				         I*y[get_LM_index(L,M)*dims+get_g_index(IMAG,L,M)];

			total+=g*conj(g);
		}

		myprintf("%f ", total);
	}

	myprintf("%f", get_norm(y));

	myprintf("\n");
}

void print_footer(double t,double y[])
{
	double expvalL=0.0f;

	for(int L=0;L<Lmax;L++)
		expvalL+=L*get_norm_L(y,L);

	myprintf("# FINAL DEBUG INFO1 <t> <Lmax> <u0> <r0> <u2> <r2> <rate> <laserU0> <L> <|g(L=0,M=0)|^2> ...<|g(L=Lmax,M=LMax)|^2>\n");
	myprintf("# FINAL DEBUG INFO2 %f %d %f %f %f %f %f %f %f ", t, Lmax, u0, r0, u2, r2, rate, parameterU0, expvalL);

	for(int L=0;L<Lmax;L++)
	{
		for(int M=-L;M<=L;M++)
		{
			double complex g=y[get_LM_index(L,M)*dims+get_g_index(REAL,L,M)]+
				I*y[get_LM_index(L,M)*dims+get_g_index(IMAG,L,M)];

			myprintf("%f ", g*conj(g));
		}
	}

	myprintf("\n");
}
