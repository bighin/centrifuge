#include <stdio.h>
#include <math.h>
#include <complex.h>
#include <assert.h>

#include <gsl/gsl_errno.h>
#include <gsl/gsl_odeiv2.h>

#include "evolution.h"
#include "centrifuge.h"
#include "output.h"

/*
	The time evolution function for a single (L,M) state
*/

struct params_t
{
	int L,M;
	int lambda,q,qprime;
};

int time_evolution(double t,const double y[],double dydt[],void *params)
{
	struct params_t *localparams=(struct params_t *)(params);

	int L,M;

	L=localparams->L;
	M=localparams->M;

	/*
		Do we actually need to reset dydt to zero? We do it just in case...
	*/

	for(int c=0;c<dims;c++)
		dydt[c]=0.0f;

	double Omega;
	double complex g,dgdt;

	Omega=rate*t;

	g=y[get_g_index(REAL,L,M)]+I*y[get_g_index(IMAG,L,M)];
	dgdt=-I*(L*(L+1)-Omega*M-parameterU0/3.0f)*g;

	for(int lambda=0;lambda<=2;lambda+=2)
	{
		for(int j=L-2;j<=L+2;j+=2)
		{
			if(j<0)
				continue;

			if((lambda==0)&&(j!=L))
				continue;

			for(int q=0;q<qsteps;q++)
			{
				double complex beta;
				double coupling;

				beta=y[get_beta_index(REAL,L,M,lambda,j,q)]+I*y[get_beta_index(IMAG,L,M,lambda,j,q)];
				coupling=cg(j,0,lambda,0,L,0)*sqrt((2.0f*j+1.0f)*(2.0f*lambda+1.0f)/(2.0f*L+1.0f)/(4.0f*M_PI));

				dgdt+=-I*falpha(lambda,q)*coupling*beta;
			}
		}
	}

	dydt[get_g_index(REAL,L,M)]=creal(dgdt);
	dydt[get_g_index(IMAG,L,M)]=cimag(dgdt);

	for(int q=0;q<qsteps;q++)
	{
		for(int lambda=0;lambda<=2;lambda+=2)
		{
			for(int j=L-2;j<=L+2;j+=2)
			{
				if(j<0)
					continue;

				if((lambda==0)&&(j!=L))
					continue;

				double complex beta,dbetadt;

				beta=y[get_beta_index(REAL,L,M,lambda,j,q)]+I*y[get_beta_index(IMAG,L,M,lambda,j,q)];
				dbetadt=-I*(j*(j+1)-Omega*M-parameterU0/3.0f)*beta;

				for(int qprime=0;qprime<qsteps;qprime++)
					dbetadt+=-I*fgamma(q,qprime)*beta;

				double coupling=cg(j,0,lambda,0,L,0)*sqrt((2.0f*j+1.0f)*(2.0f*lambda+1.0f)/(2.0f*L+1.0f)/(4.0f*M_PI));

				dbetadt+=-I*falpha(lambda,q)*coupling*g;

				dydt[get_beta_index(REAL,L,M,lambda,j,q)]=creal(dbetadt);
				dydt[get_beta_index(IMAG,L,M,lambda,j,q)]=cimag(dbetadt);
			}
		}
	}

	return GSL_SUCCESS;
}

double A(int L,int M,int Lprime,int Mprime,int fancyL,int fancyM)
{
	double tensorA=0.0f;

	if((fancyL==0)&&(fancyM==0))
		tensorA=2.0f*sqrt(M_PI)/3.0f;

	else if((fancyL==2)&&(fancyM==-2))
		tensorA=sqrt(2.0f*M_PI/15.0f);

	else if((fancyL==2)&&(fancyM==0))
		tensorA=-(2.0f/3.0f)*sqrt(M_PI/5.0f);

	else if((fancyL==2)&&(fancyM==2))
		tensorA=sqrt(2.0f*M_PI/15.0f);

	return -parameterU0*sqrt((2.0f*fancyL+1.0f)*(2.0f*Lprime+1.0f)/(2.0f*L+1.0f)/(4.0f*M_PI))*cg(Lprime,Mprime,fancyL,fancyM,L,M)*cg(Lprime,0,fancyL,0,L,0)*tensorA;
}

int laser_time_evolution(double t,const double y[],double dydt[],void *params __attribute__((unused)),int L1,int L2)
{
	double Omega=rate*t;

	for(int L=L1;L<L2;L++)
	{
		for(int M=-L;M<=L;M++)
		{
			double complex dgdt=0.0f;
			int offset=get_LM_index(L,M)*dims;

			for(int Lprime=L1;Lprime<L2;Lprime++)
			{
				if(abs(L-Lprime)>2)
					continue;

				for(int Mprime=-Lprime;Mprime<=Lprime;Mprime++)
				{
					int offsetprime=get_LM_index(Lprime,Mprime)*dims;

					double complex gprime;

					gprime=y[offsetprime+get_g_index(REAL,Lprime,Mprime)]+I*y[offsetprime+get_g_index(IMAG,Lprime,Mprime)];

					dgdt+=-I*gprime*cexp(I*Omega*(M-Mprime)*t)*A(L,M,Lprime,Mprime,0,0);
					dgdt+=-I*gprime*cexp(I*Omega*(M-Mprime)*t)*A(L,M,Lprime,Mprime,2,-2);
					dgdt+=-I*gprime*cexp(I*Omega*(M-Mprime)*t)*A(L,M,Lprime,Mprime,2,0);
					dgdt+=-I*gprime*cexp(I*Omega*(M-Mprime)*t)*A(L,M,Lprime,Mprime,2,2);
				}
			}

			dydt[offset+get_g_index(REAL,L,M)]+=creal(dgdt);
			dydt[offset+get_g_index(IMAG,L,M)]+=cimag(dgdt);
		}
	}

	for(int L=L1;L<L2;L++)
	{
		for(int M=-L;M<=L;M++)
		{
			int offset=get_LM_index(L,M)*dims;

			for(int q=0;q<qsteps;q++)
			{
				for(int lambda=0;lambda<=2;lambda+=2)
				{
					for(int j=L-2;j<=L+2;j+=2)
					{
						if(j<0)
							continue;

						if((lambda==0)&&(j!=L))
							continue;

						double complex dbetadt=0.0f;

						for(int Lprime=L1;Lprime<L2;Lprime++)
						{
							if(abs(L-Lprime)>2)
								continue;

							for(int Mprime=-Lprime;Mprime<=Lprime;Mprime++)
							{
								int offsetprime=get_LM_index(Lprime,Mprime)*dims;

								for(int jprime=Lprime-2;jprime<=Lprime+2;jprime+=2)
								{
									double complex z,betaprime;

									if(jprime<0)
										continue;

									if((lambda==0)&&(jprime!=Lprime))
										continue;

									betaprime=y[offsetprime+get_beta_index(REAL,Lprime,Mprime,lambda,jprime,q)]+I*y[offsetprime+get_beta_index(IMAG,Lprime,Mprime,lambda,jprime,q)];

									z=-I*betaprime*cexp(I*Omega*(M-Mprime)*t);

									for(int mu=-2;mu<=2;mu++)
									{
										dbetadt+=z*A(j,M-mu,jprime,Mprime-mu,0,0);
										dbetadt+=z*A(j,M-mu,jprime,Mprime-mu,2,-2);
										dbetadt+=z*A(j,M-mu,jprime,Mprime-mu,2,0);
										dbetadt+=z*A(j,M-mu,jprime,Mprime-mu,2,2);
									}
								}
							}
						}

						dydt[offset+get_beta_index(REAL,L,M,lambda,j,q)]+=creal(dbetadt);
						dydt[offset+get_beta_index(IMAG,L,M,lambda,j,q)]+=cimag(dbetadt);
					}
				}
			}
		}
	}

	return GSL_SUCCESS;
}

/*
	The time evolution function called by the GSL integrator
*/

int full_time_evolution(double t,const double y[],double dydt[],void *params)
{
	struct params_t localparams;

	/*
		Is this actually needed? Does GSL do it for us?
	*/

	for(int c=0;c<dims*Lmax*Lmax;c++)
		dydt[c]=0.0f;

	for(int L=0;L<Lmax;L++)
	{
		for(int M=-L;M<=L;M++)
		{
			int offset=get_LM_index(L,M)*dims;

			localparams.L=L;
			localparams.M=M;

			time_evolution(t,&y[offset],&dydt[offset],&localparams);
		}
	}

	laser_time_evolution(t,y,dydt,params,0,Lmax);

	return GSL_SUCCESS;
}

void do_dynamics(int nr_steps, double ti, double tf)
{
	assert(dims>0);

	/*
		Note that dims is the dimensions of one (L,M) state, and we have

		\sum_{L=0}^{Lmax-1} 2L + 1 =  Lmax^2

		of those states.
	*/

	double *y=malloc(sizeof(double)*dims*Lmax*Lmax);

	/*
		Here we set the initial state: g{startL,startM}=1
	*/

	int targetc=get_LM_index(startL,startM)*dims+0;

	for(int c=0;c<dims*Lmax*Lmax;c++)
		y[c]=(c==targetc)?(1.0f):(0.0f);

	gsl_odeiv2_system sys={full_time_evolution,NULL,dims*Lmax*Lmax,y};

#warning Here and in restricted.c probably the precision could be decreased a bit.

	gsl_odeiv2_driver *d=gsl_odeiv2_driver_alloc_y_new(&sys,gsl_odeiv2_step_rkf45,1e-6,1e-6,0.0);

	double t=ti;

	print_first_line(t, y);
	print_entry_line(t,y);

	for(int c=1;c<nr_steps;c++)
	{
		double tnext=ti+c*(tf-ti)/nr_steps;
		int status;

		if((status=gsl_odeiv2_driver_apply(d,&t,tnext,y))!=GSL_SUCCESS)
		{
			printf("GSL error, return value=%d\n",status);
			break;
		}

		/*
			After each step we output some observables...
		*/

		print_entry_line(t,y);
	}

	print_footer(t,y);

	gsl_odeiv2_driver_free(d);

	if(y)
		free(y);
}
