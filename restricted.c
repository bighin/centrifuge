#include <stdio.h>
#include <complex.h>
#include <assert.h>

#include <gsl/gsl_errno.h>
#include <gsl/gsl_odeiv2.h>

#include "restricted.h"
#include "centrifuge.h"
#include "evolution.h"
#include "output.h"

/*
	Here we have 'restricted' dynamics, i.e. dynamics in which
	only states between a certain value L1 and a certain other value L2
	are evolved in time.

	Of course, we must be sure that the spectral weight of the other
	states is very close to zero.
*/

/*
	The criteria for determining L1 and L2.
*/

int find_L1(const double y[])
{
	int L;

	for(L=0;L<Lmax;L++)
		if(get_norm_L(y,L)>1e-5)
			break;

	if(L>=Lmax)
		return Lmax-1;

	return L;
}

int find_L2(const double y[])
{
	int ret=find_L1(y)+4;

	if(ret>=Lmax)
		return Lmax-1;

	return ret;
}

/*
	The 'restricted' time evolution function and the context used to pass arguments to it.
*/

struct Lrestricted_params_t
{
	int L1,L2,offset;
};

struct params_t
{
	int L,M;
	int lambda,q,qprime;
};

int full_time_evolution_Lrestricted(double t,const double Lrestricted_y[],double Lrestricted_dydt[],void *params)
{
	int L1,L2,offset;

	L1=((struct Lrestricted_params_t *)(params))->L1;
	L2=((struct Lrestricted_params_t *)(params))->L2;
	offset=((struct Lrestricted_params_t *)(params))->offset;

	assert(L1>=0);
	assert(L1<Lmax);
	assert(L2>=0);
	assert(L2<Lmax);
	assert(L2>L1);

	double *y=malloc(sizeof(double)*dims*Lmax*Lmax);
	double *dydt=malloc(sizeof(double)*dims*Lmax*Lmax);

	/*
		We reset the state vector and its derivative
	*/

	for(int c=0;c<dims*Lmax*Lmax;c++)
	{
		y[c]=0.0f;
		dydt[c]=0.0f;
	}

	/*
		We copy the 'restricted' state vector and its derivative
		to the full state vector...
	*/

	for(int c=0;c<dims*(L2*L2-L1*L1);c++)
	{
		y[offset+c]=Lrestricted_y[c];
		dydt[offset+c]=Lrestricted_dydt[c];
	}

	/*
		...then we apply the usual evolution
	*/

	for(int L=L1;L<L2;L++)
	{
		for(int M=-L;M<=L;M++)
		{
			int localoffset=get_LM_index(L,M)*dims;

			struct params_t localparams;

			localparams.L=L;
			localparams.M=M;

			time_evolution(t,&y[localoffset],&dydt[localoffset],&localparams);
		}
	}

	laser_time_evolution(t,y,dydt,NULL,L1,L2);

	for(int c=0;c<dims*(L2*L2-L1*L1);c++)
		Lrestricted_dydt[c]=dydt[offset+c];

	if(y)
		free(y);

	if(dydt)
		free(dydt);

	return GSL_SUCCESS;
}

/*
	A 'slice', i.e. a portion of the time evolution with fixed L1 and L2,
	after which L1 and L2 are recalculated.
*/

void do_Lrestricted_dynamics_slice(int L1,int L2,double y[],double ti,double tf,int startstep,int endstep,int nr_steps)
{
	/*
		Here we have some differences: since we consider only states with angular momentum
		between L1 (included) and L2 (excluded), the dimensions are:

		\sum_{L=L1}^{L2} 2L + 1 =  L2^2 - L1^2

		and restricted_offset is the offset of the first relevant L-state.
	*/

	int restricted_offset=get_LM_index(L1,-L1)*dims;

	struct Lrestricted_params_t rpt;

	rpt.L1=L1;
	rpt.L2=L2;
	rpt.offset=restricted_offset;

	gsl_odeiv2_system sys={full_time_evolution_Lrestricted,NULL,dims*(L2*L2-L1*L1),&rpt};
	gsl_odeiv2_driver *d=gsl_odeiv2_driver_alloc_y_new(&sys,gsl_odeiv2_step_rkf45,1e-6,1e-6,0.0);

	double t=ti+startstep*(tf-ti)/nr_steps;

	if(startstep==0)
	{
		print_first_line(t, y);
		print_entry_line(t,y);
	}

	for(int c=startstep+1;c<=endstep;c++)
	{
		double tnext=ti+c*(tf-ti)/nr_steps;
		int status;

		if((status=gsl_odeiv2_driver_apply(d,&t,tnext,&(y[restricted_offset])))!=GSL_SUCCESS)
		{
			printf("GSL error, return value=%d\n",status);
			break;
		}

		/*
			After each step we output some observables...
		*/

		print_entry_line(t,y);
	}

	gsl_odeiv2_driver_free(d);
}

void do_Lrestricted_dynamics(int nr_steps, double ti, double tf)
{
	assert(dims>0);

	/*
		We still allocate the full vector, with dims*Lmax*Lmax entries.
	*/

	double *y=malloc(sizeof(double)*dims*Lmax*Lmax);

	/*
		Here we set the initial state: g{startL,startM}=1
	*/

	int targetc=get_LM_index(startL,startM)*dims+0;

	for(int c=0;c<dims*Lmax*Lmax;c++)
		y[c]=(c==targetc)?(1.0f):(0.0f);

	int stepsperslice=25;

	for(int c=0;c<nr_steps;c+=stepsperslice)
	{
		int L1,L2;

		L1=find_L1(y);
		L2=find_L2(y);

		myprintf("# DEBUG INFO: doing a slice from %d to %d steps.\n", c, c+stepsperslice);
		myprintf("# DEBUG INFO: L1=%d, L2=%d\n", L1, L2);
		myprintf("# DEBUG INFO: steps=%d/%d\n", c, nr_steps);

		{
			double inside,outside,pct;

			inside=outside=0.0f;

			for(int L=0;L<Lmax;L++)
			{
				outside+=get_norm_L(y,L);

				if((L>=L1)&&(L<=L2))
					inside+=get_norm_L(y,L);
			}

			pct=inside/outside*100.0f;

			myprintf("# DEBUG INFO: The interval chosen contains %f%% of the spectral weight.\n", pct);
		}

		do_Lrestricted_dynamics_slice(L1, L2, y, ti, tf, c, c+stepsperslice, nr_steps);
	}

	print_footer(tf,y);
}
