#ifndef __BASIS_H__
#define __BASIS_H__

#define KBASIS_GRID	(31)
#define KBASIS_LEGENDRE	(32)
#define KBASIS_HERMITE	(33)
#define KBASIS_BOXES	(34)

double falpha_grid(int lambda,int q);
double fgamma_grid(int q,int qprime);

double falpha_basis(int lambda, int q);
double fgamma_basis(int q, int qprime);

double check_normalization(int q,int qprime);

void init_basis_tables(void);
void fini_basis_tables(void);

const char *get_basis_type(void);

#endif //__BASIS_H__
