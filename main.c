#include "centrifuge.h"
#include "evolution.h"
#include "restricted.h"
#include "basis.h"
#include "output.h"

int main(int argc,char **argv)
{
	if(argc<2)
	{
		printf("Usage: %s <inifile> [<otherinifiles> ...]\n",argv[0]);
		return 0;
	}

	printf("# Dimensions of CG tables: %f MiB\n",((float)(cg_tables_dimensions()))/1024/1024);
	cg_fill_tables();

	for(int c=1;c<argc;c++)
	{
		if(do_ini_file(argv[c])==false)
			continue;

		dims=2+qsteps*8;

		if((kbasis==KBASIS_LEGENDRE)||(kbasis==KBASIS_HERMITE)||(kbasis==KBASIS_BOXES))
			init_basis_tables();

		print_header();

		if(restricted==true)
			do_Lrestricted_dynamics(nrsteps, tstart, tend);
		else
			do_dynamics(nrsteps, tstart, tend);

		/*
			Final cleaning up
		*/

		if((kbasis==KBASIS_LEGENDRE)||(kbasis==KBASIS_HERMITE)||(kbasis==KBASIS_BOXES))
			fini_basis_tables();
	}
}
