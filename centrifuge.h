#ifndef __CENTRIFUGE_H__
#define __CENTRIFUGE_H__

#include <stdio.h>
#include <stdbool.h>

extern int Lmax;
extern int startL,startM;
extern int qsteps;
extern double qcutoff;
extern int dims;

extern int kbasis;

extern double n;
extern double BinGHz;
extern double u0,r0;
extern double u1,r1;
extern double u2,r2;

extern double parameterU0,rate;

extern int nrsteps;
extern double tstart,tend;
extern bool restricted;

extern char *inipath,*prefix;
extern FILE *outfile;

double wigner3j(int l1,int l2,int l3,int m1,int m2,int m3);
double cg(int j1,int m1,int j2,int m2,int j,int m);

int cg_tables_dimensions(void);
bool cg_fill_tables(void);

double ek(double k);
double omegak(double k);

double U(int lambda,double k);

#define REAL	(21)
#define IMAG	(22)

int get_g_index(int type,int L,int M);
int get_beta_index(int type,int L,int M,int lambda,int j,int q);

double falpha(int lambda,int q);
double fgamma(int q,int qprime);

int get_LM_index(int L,int M);

double get_qpw_L(const double y[],int L,int M);
double get_qpw(const double y[]);

double get_energy_L(const double y[],int L,int M);
double get_energy(const double y[]);

double get_norm_LM(const double y[],int L,int M);
double get_norm_L(const double y[],int L);
double get_norm(const double y[]);

void load_configuration_defaults(void);
bool do_ini_file(char *inifile);

#endif //__CENTRIFUGE_H__
